#!/usr/bin/env python

from requests import post, codes, get
from logging import debug, basicConfig, error, info, DEBUG, exception

from aop_lib.ourConfig import getConfig
from aop_lib.manageAccessTokens import getAccessToken

from sys import exit

basicConfig(level=DEBUG)

def getFileList(org, productID):

    ourConfig = getConfig(productID)

    apiKey = ourConfig.get("our-api-key")
    apiVersion = ourConfig.get("api-version")
    apiRoot = ourConfig.get('api-root')
    exportFormat = ourConfig.get('export-format')
    debug(f"export format is {exportFormat}")

    resp = post( f"{apiRoot}/{org}/exports/{apiVersion}/job-logs",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"},
            json = {"format":exportFormat}
            )

    debug(f"called API export joblogs and response was {resp}")
    
    if not resp.ok :
        exception( f"API Call export job logs failed for org {org}, \n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return resp.json()
