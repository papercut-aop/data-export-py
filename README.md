# PaperCut Cloud native Add-on platform demo program

`getContent.py` -- uses the data export API methods to get historical data

**NOTE**: This program uses submodules. Clone with the `--recurse-submodules` option. e.g.

```
git clone --recurse-submodules git@gitlab.com:papercut-aop/data-export-py.git
```

Platform docs are [here](https://papercut-aop.gitlab.io/api-docs/)
