# Notes

## My Notes:

Build Hugo content

```
SPEC_TO_DISPLAY="pmitc-api.yaml"
npm install swagger-ui-dist
mkdir -p docs/static/API-reference/
cp -a node_modules/swagger-ui-dist/* docs/static/API-reference/
sed -i "s#https://petstore\.swagger\.io/v2/swagger\.json#$SPEC_TO_DISPLAY#gp" docs/static/API-reference/index.html
for f in docs/content/*plantuml ; do plantuml -overwrite -tpng  -output ./docs/static $f ; done
docker container run --rm -it --user 1000:1000 -p 1313:1313 --mount type=bind,source=$PWD,target=/src registry.gitlab.com/pages/hugo:latest hugo -s docs -d ../public serve -D --bind "0.0.0.0"
```

Delete old Pipelines. See https://stackoverflow.com/questions/53355578/how-to-delete-gitlab-ci-jobs-pipelines-logs-builds-and-history

## Setup Process

1. Register as a Hive user organisation
2. Complete the developer onboarding form
3. Receive a API key from PaperCut
4. Make sure you have a server with a valid TLS cert (see notes below for self signed certificate for your development server)

## Testing

5. Log into Hive admin interface and select add-ons. Your should see the new add-on (it's only shown in your developer org)
6. Test your new API key. See `testApiToken.py` example. You should see something like `{"ok":false,"error":"invalid_token"}`, note that if org ID is incorrect you will also see this error message. Note if your API token is invalid the response is `{"ok":false,"error":"invalid_auth"}`

## Using Self Signed certificates for development

**These notes assume you are using a pem format cert**

1. Create a certificate and key pair (you can use our cert-tool)
2. Add the #TODO....

## TODO

1. Need better diagnostic output for external devs
2. What to do about TLS certs? Should we allow devs to upload self-signed certs?
4. API call to "discover all the info about an org -- Name, billing status, ...
5. API Rate call throttling
