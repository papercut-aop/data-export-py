#!/usr/bin/env python

# A simple example of PaperCut Cloud Native Add-On implementation

# Note this is not production code. It's designed to show what needs to be implemented in a simple manner

from requests import get
from logging.config import dictConfig
from logging import critical, info, debug, exception, basicConfig, DEBUG
from dateutil.parser import parse
from datetime import datetime, timedelta
from pytz import utc
from json import loads as JSONloads
from jsonstreams import Stream as JSONstream, Type as JSONtype  # Write JSON stream
from csv import DictReader
from sys import exit
from time import sleep
from os import mkdir, write

from aop_lib.ourConfig import getConfig
from aop_lib.ourDatabase import getCustomerList, updateLastProcessDate
from AOP_APIwrappers import getFileList

basicConfig( level=DEBUG)

ourConfig = getConfig("DataExport")  # hard coded because that is all this program does

debug("Starting get content")

try:
    mkdir(ourConfig["data-dir"])
except:
    pass

def processJobRecord(jobRecord):
    if jobRecord['status'] in ["completed", "cancelled"]:
        return jobRecord
    return None


def processFile(fileInfo, org, lastProcessDate: datetime) -> datetime:

    eventsTo = parse( fileInfo['eventsTo'])

    if eventsTo - lastProcessDate < timedelta(seconds=10):
        debug(f"Already processed url {fileInfo['url']}")
        return None

    eventsToStr = eventsTo.strftime('%Y-%m-%d-%H-%M')

    fileName = f"{ourConfig['data-dir']}/dump_{org['org-name']}_{fileInfo['jobType']}_{eventsToStr}.json"

    debug( f'Output going to {fileName}')

    alreadyTried = False

    with get( fileInfo['url'], stream=True) as resp, \
         JSONstream( JSONtype.array, filename=fileName, indent=2, pretty=True) as out:

        if not resp.ok and not alreadyTried:
            info( f"get failed. {resp}, backing off 10 seconds")
            sleep(10)
            resp = get( fileInfo['url'], stream=True)
            alreadyTried = True
            if not resp.ok:
                debug( f"file info is {fileInfo}")
                critical( f"""get from {fileInfo['url']} failed. {resp},
                           {resp.text},
                            file info is {fileInfo}""")
                return None

        if int(resp.headers['Content-Length']) == 0:
            info(f"Zero length content. No processing required")
            # Note: This creates an empty file (JSON array) on purpose
            return None

        resp.encoding = 'utf-8'

        exportFormat = ourConfig.get('export-format')

        if exportFormat is None or exportFormat.upper()  == 'CSV':
            csvReader = DictReader( resp.iter_lines(decode_unicode=True))

            for line in csvReader:
                out.write(line)

        elif exportFormat.upper()  == 'JSON':

            # It's streaming JSON lines so easiest to interate by line
            for line in resp.iter_lines(decode_unicode=True):
                jobInfo = JSONloads(line)
                if (updatedJobInfo := processJobRecord(jobInfo)) is not None:
                   out.write(updatedJobInfo)
                else:
                    debug(f"did not save job {jobInfo['jobId']}. Status is  {jobInfo['status']}")

        else:
            critical(f"unsupported file export format {exportFormat}")

    return eventsTo


def getNewFiles():

    orgList = getCustomerList(u'DataExport', ourConfig['firebase-info'])

    if orgList is None:
        info("No customers to process")
        return

    debug(f"org list is {orgList}")

    for org in orgList:

        debug(f"current org being processed is {org}")

        if  org['myProductId'] != "DataExport":
            continue

        info( f"Getting data for org {org['org-name']}")

        payload = getFileList(org['orgId'], u'DataExport')

        debug(f"called getFileList, payload is {payload}")

        sortedFileList = payload['files']
        
        sortedFileList.sort(key = lambda f: f['eventsTo'])

        debug(f"sortedFileList is {sortedFileList}")
        for file in sortedFileList:

            try:
                if (lastProcessDate := org['dataExportSettings']['lastProcessDate'][file['jobType']]) is None: 
                    lastProcessDate = utc.localize(datetime.fromtimestamp(0))

                debug(f"lastProcessDate date found in db value {lastProcessDate}")
            except KeyError:
                lastProcessDate = utc.localize(datetime.fromtimestamp(0))  # Epoch
                debug(f"lastProcessDate date NOT found in db set to {lastProcessDate}")

            debug(f"processing file: {file}")

            if (lastSeen := processFile(file, org, lastProcessDate)) is not None:
                updateLastProcessDate(org['orgId'], lastSeen, file['jobType'], ourConfig['firebase-info'])

if __name__ == "__main__":
    while True:
        sleep(3)
        getNewFiles()